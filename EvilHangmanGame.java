import java.io.File;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.HashMap;
import java.util.Scanner;
import java.io.FileNotFoundException;

/**
 * Created by Kaiden on 1/24/2018.
 */

public class EvilHangmanGame implements IEvilHangmanGame {

    private Set<String> wordPool = new HashSet<String>();
    private boolean[] guessedLetters = new boolean[26];
    private StringBuilder word = new StringBuilder();
    private StringBuilder previousWord = new StringBuilder();

    public EvilHangmanGame() {

    }

    public static void main(String[] args) {
        if (args.length < 3) {
            System.out.println("Invalid command line arguments.");
            System.out.println("Usage: java EvilHangmanGame dictionary wordLength guesses");
            System.out.println("Please enter valid command line arguments and try again.");
            return;
        }
        EvilHangmanGame game = new EvilHangmanGame();
        File file = new File(args[0]);

        String wordLengthString = args[1];
        String wordGuessesString = args[2];

        int wordLength;
        int wordGuesses;
        try {
            wordLength = Integer.parseInt(wordLengthString);
            wordGuesses = Integer.parseInt(wordGuessesString);
        } catch (Exception ex) {
            System.out.println("Word length and guesses must be valid integers.");
            return;
        }

        if (!(wordLength > 1)) {
            System.out.println("Invalid word length. Word length must be 2 or greater.");
            return;
        }

        if (!(wordGuesses > 0)) {
            System.out.println("Invalid number of guesses. Guesses must be 1 or greater.");
            return;
        }

        game.startGame(file, wordLength);

        if (!(game.wordPool.size() > 0)) {
            System.out.println("The dictionary is empty. Please use a valid dictionary file.");
            return;
        }

        game.beginGameplay(wordGuesses);
    }

    @Override
    public void startGame(File dictionary, int wordLength) {
        try (Scanner scan = new Scanner(dictionary)) {
            StringBuilder content = new StringBuilder();
            while (scan.hasNextLine()) {
                content.append(scan.nextLine());
                if (content.length() == wordLength) {
                    wordPool.add(content.toString().toLowerCase());
                }
                content.setLength(0);
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Could not find file: " + dictionary);
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument: Fix your bug!");
        } catch (Exception ex) {
            System.out.println("Error: " + ex.getMessage());
        }
        for (int i = 0;i< wordLength;i++) {
            word.append('-');
            previousWord.append('-');
        }
    }

    private void beginGameplay(int wordGuesses) {
        if (word.toString().indexOf('-') == -1) {
            System.out.println("You win!");
            System.out.println("You found the word: " + word.toString());
            return;
        }
        if (wordGuesses == 0) {
            System.out.println("You lose!");
            System.out.println("The word was: " + chooseWord());
            return;
        }
        System.out.println("You have " + wordGuesses + " guesses left." );
        System.out.print("Used letters: ");
        for (int i = 0;i<26;i++) {
            if (guessedLetters[i] == true) {
                char guessedLetter = (char) (i + 'a');
                System.out.print(guessedLetter + " ");
            }
        }
        System.out.println();
        System.out.println("Word: " + word.toString());

        char guess = userGuess();

        System.out.println();

        int numberOfLettersSuccessfullyGuessed = getDifferenceOfGuess();
        if (numberOfLettersSuccessfullyGuessed == 0) {
            System.out.println("Sorry, there are no " + guess + "'s.");
            --wordGuesses;
        } else if (numberOfLettersSuccessfullyGuessed == 1) {
            System.out.println("Yes, there is " + numberOfLettersSuccessfullyGuessed + " " + guess + "'s");
        } else {
            System.out.println("Yes, there are " + numberOfLettersSuccessfullyGuessed + " " + guess + "'s");
        }

        previousWord.setLength(0);
        previousWord.append(word.toString());
        System.out.println();
        beginGameplay(wordGuesses);
    }

    @Override
    public Set<String> makeGuess(char guess) throws GuessAlreadyMadeException {
        int guessedLetterIndex = guess - 'a';
        if (guessedLetters[guessedLetterIndex] == false) {
            guessedLetters[guessedLetterIndex] = true;
        }
        else {
            throw new GuessAlreadyMadeException();
        }
        Map<String, Set<String>> partitionedWordPool = new HashMap<String, Set<String>>();
        for (String poolWord : wordPool) {
            StringBuilder pattern = new StringBuilder();
            for (int i =0;i<poolWord.length();i++) {
                if (poolWord.charAt(i) == guess) {
                    pattern.append(guess);
                }
                else {
                    pattern.append(word.toString().charAt(i));
                }
            }
            Set<String> words = partitionedWordPool.get(pattern.toString());
            if (words == null) {
                words = new HashSet<String>();
            }
            words.add(poolWord);
            partitionedWordPool.put(pattern.toString(), words);
        }
        return chooseLargestWordSet(partitionedWordPool, guess);
    }

    private String chooseWord() {
        for (String word : wordPool) {
            return word;
        }
        return null;
    }

    private char getGuess() {
        System.out.print("Enter guess: ");
        // Get input
        String character = System.console().readLine().toLowerCase();
        char guess;
        if (character.length() == 1 &&  Character.isLetter(character.charAt(0))) {
            guess = character.toLowerCase().charAt(0);
        }
        else {
            System.out.println("Invalid input. Please enter a valid character (a-z)");
            return getGuess();
        }
        return guess;
    }

    private int getDifferenceOfGuess() {
        int difference = 0;
        for (int i =0; i< word.length();i++) {
            if (word.charAt(i) != previousWord.charAt(i)) {
                difference++;
            }
        }
        return difference;
    }

    private char userGuess() {
        System.out.println();
        char guess = getGuess();
        try {
            wordPool = makeGuess(guess);
        } catch (GuessAlreadyMadeException ex) {
            System.out.println("You already used that letter. Please choose a different letter");
            userGuess();
        }
        return guess;
    }

    private Set<String> chooseLargestWordSet(Map<String, Set<String>> partitionedWordPool, char guess) {
        StringBuilder largestPatternSet = new StringBuilder();
        int numberOfLargestSet = 0;
        for ( Map.Entry<String, Set<String>> entry : partitionedWordPool.entrySet()) {
            int numberOfEntrySet = entry.getValue().size();
            // If this set is bigger than the other, use that set
            if (numberOfEntrySet > numberOfLargestSet) {
                numberOfLargestSet = numberOfEntrySet;
                largestPatternSet.setLength(0);
                largestPatternSet.append(entry.getKey());
            } else if (numberOfEntrySet == numberOfLargestSet) {
                // If the current set does have the letter and the new one doesn't, update the set
                if (largestPatternSet.toString().indexOf(guess) != -1 && entry.getKey().indexOf(guess) == -1) {
                    numberOfLargestSet = numberOfEntrySet;
                    largestPatternSet.setLength(0);
                    largestPatternSet.append(entry.getKey());
                }
                // If the current set doesn't have the letter, but the new one does, move on.
                else if (largestPatternSet.toString().indexOf(guess) == -1 && entry.getKey().indexOf(guess) != -1) {
                    continue;
                }
                // They either both have the letter, or both don't have it
                else {
                    int currentSetCount = 0;
                    int newSetCount = 0;

                    for (int i = 0; i < largestPatternSet.toString().length(); i++) {
                        if (largestPatternSet.toString().charAt(i) == guess) {
                            currentSetCount++;
                        }
                        if (entry.getKey().charAt(i) == guess) {
                            newSetCount++;
                        }
                    }

                    if (newSetCount < currentSetCount) {
                        numberOfLargestSet = numberOfEntrySet;
                        largestPatternSet.setLength(0);
                        largestPatternSet.append(entry.getKey());
                        continue;
                    }

                    if (currentSetCount < newSetCount) {
                        continue;
                    }

                    for(int i = largestPatternSet.toString().length()-1; i >= 0; i--){
                        if (largestPatternSet.toString().charAt(i) != guess && entry.getKey().charAt(i) == guess) {
                            numberOfLargestSet = numberOfEntrySet;
                            largestPatternSet.setLength(0);
                            largestPatternSet.append(entry.getKey());
                            break;
                        } else if (largestPatternSet.toString().charAt(i) == guess && entry.getKey().charAt(i) != guess) {
                            break;
                        }
                    }
                }
            }
        }
        word.setLength(0);
        word.append(largestPatternSet.toString());
        return partitionedWordPool.get(largestPatternSet.toString());
    }
}
